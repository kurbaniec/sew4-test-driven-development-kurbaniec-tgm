# Test Driven Development in Python

## Aufgabenstellung
Schreiben Sie die notwendigen Methoden zur Implementierung der Bruch-Klasse und testen Sie diese mit den beigefügten Tests.

## Bewertung
### Grundanforderungen
* README.md mit Testreports (inkl. Coverage)
* Dokumentation (html)
* Source Code (Python-files)
* Test Coverage von zumindest 80%
* Achtung: Vergessen Sie nicht auf eine ausführliche Dokumentation mittels Sphinx

### Erweiterungen
* Test Coverage von zumindest 95%
* Erweiterte Bruch-Klasse inkl. eigener Unit-Tests zum Unterstützen von float-Zahlen als Zähler und Nenner
* Fehlerbehandlungen


### Umgebung und Tests
Das Beispiel soll entsprechend der mitgelieferten Tests implementiert werden. Folgende Befehle werden bei der Abnahme ausgeführt und bewertet:

    tox

## Umsetzung
<Was musste recherchiert werden um die Aufgabe lösen zu können? Welche Erfahrungen wurden gemacht? Wie ist man zum Ergebnis gekommen?> <br>
Am Anfang war ich ein bisschen irritiert, wieso die ganzen Methoden mit __ anfangen und enden. Ich wusste es hat was mit Klassen zu tun, wegen der selben
Syntax wie die \_\_init__- oder \_\_repr__-Methode, die man im Codecademy Kurs schon kennengelernt hat. Außerdem habe ich nicht gewusst, 
wie mir diese Methoden mit den ganzen Operatoren helfen sollen.
Die Antworten auf meine Fragen habe ich auf dieser [Website ^1^](https://www.python-course.eu/python3_magic_methods.php) gefunden, 
die erkärt, dass es sich um "Magic-Methoden" handelt, also Methoden die bestimmte Operationen definieren.

Danach war das Schreiben der Klasse nicht wirklich schwierig, wo ich aber ein biscchen reingefallen bin, war die rsub-Methode. Bei einer
Subtrakion ist es wichtig, wer Minuend und wer Subtrahend ist. Dies musste man bei der rsub-Methode durch Negieren des Zähler und Other "vertauschen".

Um ein paar Testfälle erfolgreich abschließen zu können, musste man Brüche kürzen. Um die Testfälle erfolgreich abzuschließen reichte es in der 
equals-Methode die Brüche zu kürzen. Um aber eine vollständigere Klasse zu besitzen, habe ich es einfach im Konstruktor definiert. 
Dazu brauchte ich noch das Python-äquivalent von einer "Größter Gemeinsamer Nenner"-Methode, die ich [hier ^2^](https://stackoverflow.com/questions/11175131/code-for-greatest-common-divisor-in-python)
 gefunden haben.
 
Um die Tuple-Test erfolgreich abzuschließen musste man sich mit der iter-Methode beschäftigen. Dazu musste ich mehr recherchieren und habe diese
Seiten [^3^](https://pythontips.com/2013/09/29/the-python-yield-keyword-explained/), [^4^](https://www.geeksforgeeks.org/use-yield-keyword-instead-return-keyword-python/) gefunden.
Nachdem ich yield verstanden hatte, war das Schreiben von iter auch nicht mehr so schwer. 

Für die Erweiterung des Konstruktors, damit dieser mit float-Zahlen arbeiten kannn, habe ich mir ein einfaches Konzept überlegt, dass darauf ausgelegt ist, dass der Konstruktor aus 
float-Zahlen wie 1.02 einfach einen Bruch (102/100) erstellt. Damit kann die Klasse indirekt mit float-Werten arbeiten und die Konsistenz der anderen Methoden ist dabei auch gegeben.
Bei der Umsetzung musste ich einfach die Nachkommastellen zählen und dann Bruch und Nenner pro Nachkommastelle mit 10 multiplizieren. <br>
Durch die Erweiterung des Konstruktors für float-Zahlen werden zwei Testfälle, testcreateBruchWrongTypeNenner und testcreateBruchWrongTypeZaehler, immer als failed angezeigt, weil
float-Werte beim Standardkonstruktor eine Fehlermeldung ausgeben sollten.


## Quellen
[Repository mit Python Beispiel-Sourcecode](https://github.com/TGM-HIT/sew4_examples.git)  
[1]: https://www.python-course.eu/python3_magic_methods.php <br>
[2]: https://stackoverflow.com/questions/11175131/code-for-greatest-common-divisor-in-python <br>
[3]: https://pythontips.com/2013/09/29/the-python-yield-keyword-explained/ <br>
[4]: https://www.geeksforgeeks.org/use-yield-keyword-instead-return-keyword-python/


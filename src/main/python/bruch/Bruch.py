from decimal import *


class Bruch(object):

    def __init__(self, zaehler=None, nenner=None):
        """ Konstruktor

        :param zaehler: Numerator, Zaehler des Bruchs
        :param nenner: Denominator. Nenner des Bruchs
        :raises ZeroDivisionError: Wenn Nenner 0 ist, kann kein Bruch berechnet werden!
        """
        if nenner is None:
            nenner = 1
        if (isinstance(zaehler, int) or isinstance(zaehler, float)) and\
                (isinstance(nenner, int) or isinstance(nenner, float)):
            """ Klassenaufruf: Bruch(2,4)
            """
            # Wenn float numbers are given, they are converted to "bigger" integers, that contain their decimal places
            if zaehler != int(zaehler) or nenner != int(nenner):
                dec1, dec2 = str(zaehler)[::-1].find('.'), str(nenner)[::-1].find('.')
                if dec1 > dec2:
                    dec_places = dec1
                else:
                    dec_places = dec2
                i = 0
                while i < dec_places:
                    zaehler = round(zaehler * 10, dec_places)
                    nenner = round(nenner * 10, dec_places)
                    i += 1

            if zaehler < 0 and nenner < 0:
                zaehler = abs(zaehler)
                nenner = abs(nenner)
            # Made the elif to if, so that numbers that need the abs-method also are shorten with the gcd-method
            if nenner != 0:
                gcd = Bruch.gcd(zaehler, nenner)
                if gcd > 1:
                    self.zaehler = int(zaehler / gcd)
                    self.nenner = int(nenner / gcd)
                else:
                    self.zaehler = int(zaehler)
                    self.nenner = int(nenner)

            else:
                raise ZeroDivisionError("Division durch 0 nicht definiert!")

        elif isinstance(zaehler, Bruch):
            """ Klassenaufruf: self.b2 = Bruch(self.b)
                --> Zuweisen von Zaehler und Nenner aus dem Bruchobjekt
            """
            self.zaehler = zaehler.zaehler
            self.nenner = zaehler.nenner

            """
            Not really needed anymore, when an Denominator has no value, 1 is given him. Then the first if afterwards is
            executed, which is optimised for float and has an gcd for shortening the fracture
         
        elif isinstance(zaehler, int) and not isinstance(nenner, float):
             Klassenaufruf: Bruch(4)
                --> ohne Nenner, daher Deklarierung von Nenner = 1
            
            self.zaehler = zaehler
            self.nenner = 1
            """
        else:
            raise TypeError("Zaehler und/oder Nenner nicht zulaessig!")

    def __float__(self):
        """ Float-Method

        :return: Returns the fracture-object as a decimal number
        """
        return self.zaehler / self.nenner

    def __int__(self):
        """ Int-Method

        :return: Returns the fracture-object as an integer
        """
        return int(self.zaehler / self.nenner)

    def __str__(self):
        """ String-Method

        :return: Returns the fracture-object as a string representation
        """
        if self.nenner == 1:
            return "(" + str(self.zaehler) + ")"
        else:
            return "(" + str(self.zaehler) + "/" + str(self.nenner) + ")"

    def __add__(self, other):
        """ Add-Operator

        :return: Sum of the addition
        :raises TypeError: When float or str are handed over
        """
        if isinstance(other, int):
            return Bruch(self.zaehler + (other*self.nenner), self.nenner)
        if isinstance(other, Bruch):
            return Bruch((self.zaehler * other.nenner) + (other.zaehler * self.nenner), (self.nenner * other.nenner))
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Bruchadditionen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Bruchadditionen nicht erlaubt!")

    def __iadd__(self, other):
        """ IAdd-Operator (+=)

        :return: Sum of the addition
        :raises TypeError: When float or str are handed over
        """
        if isinstance(other, int) or isinstance(other, Bruch):
            return Bruch.__add__(self, other)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Bruchadditionen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Bruchadditionen nicht erlaubt!")

    def __radd__(self, other):
        """ Add-Operator, which is needed when an left object performs an add-operation on a fracture-object,
            that is right to it
        :return: Sum of the addition
        :raises TypeError: When float or str are handed over
        """
        if isinstance(other, int) or isinstance(other, Bruch):
            return Bruch.__add__(self, other)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Bruchadditionen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Bruchadditionen nicht erlaubt!")

    def __sub__(self, other):
        """ Sub-Operator

        :return: Difference of the subtraction
        :raises TypeError: When float or str are handed over
        """
        if isinstance(other, int):
            return Bruch(self.zaehler - (other*self.nenner), self.nenner)
        if isinstance(other, Bruch):
            return Bruch((self.zaehler * other.nenner) - (other.zaehler * self.nenner), self.nenner * other.nenner)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Bruchsubtraktionen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Bruchsubtraktionen nicht erlaubt!")

    def __isub__(self, other):
        """ ISub-Operator (-=)

        :return: Difference of the subtraction
        :raises TypeError: When float or str are handed over
        """
        if isinstance(other, int):
            return Bruch.__sub__(self, other)
        if isinstance(other, Bruch):
            return Bruch.__sub__(self, other)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Bruchsubtraktionen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Bruchsubtraktionen nicht erlaubt!")

    def __rsub__(self, other):
        """ Sub-Operator, which is needed when an left object performs an sub-operation on a fracture-object,
            that is right to it

        :return: Difference of the subtraction
        :raises TypeError: When float or str are handed over
        """
        if isinstance(other, int):
            temp = Bruch(-self.zaehler, self.nenner)
            other = - other
            return Bruch.__sub__(temp, other)
        if isinstance(other, Bruch):
            temp1 = Bruch(-self.zaehler, self.nenner)
            temp2 = Bruch(-other.zaehler, other.nenner)
            return Bruch.__sub__(temp1, temp2)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Bruchsubtraktionen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Bruchsubtraktionen nicht erlaubt!")

    def __truediv__(self, other):
        """ Divisons-Operator  (/)

        :return: Quotient of the division
        :raises TypeError: When float or str are handed over
        :raises ZeroDivsionError: When the numerator of the fracture is 0
        """
        if self.zaehler == 0:
            raise ZeroDivisionError
        if isinstance(other, int):
            return Bruch(self.zaehler, self.nenner * other)
        if isinstance(other, Bruch):
            return Bruch(self.zaehler * other.nenner, self.nenner * other.zaehler)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Bruchsubtraktionen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Bruchsubtraktionen nicht erlaubt!")

    def __itruediv__(self, other):
        """ Divisons-Operator (/=)

        :return: Quotient of the division
        :raises TypeError: When float or str are handed over
        :raises ZeroDivsionError: When the numerator of the fracture is 0
        """
        if isinstance(other, int) or isinstance(other, Bruch):
            return Bruch.__truediv__(self, other)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Bruchsubtraktionen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Bruchsubtraktionen nicht erlaubt!")

    def __rtruediv__(self, other):
        """ Divisons-Operator, which is needed when an left object performs an div-operation on a fracture-object,
            that is right to it

        :return: Quotient of the division
        :raises TypeError: When float or str are handed over
        :raises ZeroDivsionError: When the numerator of the fracture is 0
        """
        if isinstance(other, int) or isinstance(other, Bruch):
            return Bruch.__truediv__(self, other)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Bruchsubtraktionen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Bruchsubtraktionen nicht erlaubt!")

    def __mul__(self, other):
        """ Multiplication-Operator

        :return: Product of the multiplication
        :raises TypeError: When float or str are handed over
        """
        if isinstance(other, int):
            return Bruch(self.zaehler * other, self.nenner)
        if isinstance(other, Bruch):
            return Bruch(self.zaehler * other.zaehler, self.nenner * other.nenner)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Multiplikationen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Multiplikationen nicht erlaubt!")

    def __imul__(self, other):
        """ IMul-Operator (*=)

        :return: Product of the multiplication
        :raises TypeError: When float or str are handed over
        """
        if isinstance(other, int) or isinstance(other, Bruch):
            return Bruch.__mul__(self, other)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Multiplikationen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Multiplikationen nicht erlaubt!")

    def __rmul__(self, other):
        """ Multiplication-Operator, which is needed when an left object performs an mul-operation on a fracture-object,
            that is right to it

         :return: Product of the multiplication
         :raises TypeError: When float or str are handed over
         """
        if isinstance(other, int) or isinstance(other, Bruch):
            return Bruch.__mul__(self, other)
        if isinstance(other, float):
            raise TypeError("Rationale Zahlen sind bei Multiplikationen nicht erlaubt!")
        if isinstance(other, str):
            raise TypeError("Strings sind bei Multiplikationen nicht erlaubt!")

    def __abs__(self):
        """ Abs-Method

        :return: Returns the fracture with absolute values only
        """
        self.zaehler = abs(self.zaehler)
        self.nenner = abs(self.nenner)
        return self

    def __invert__(self):
        """ Invert-Method (~)

        :return: Returns the fracture inverted, the value of the numerator gets the value form the denominator,
                denominator gets the value from the numerator
        """
        temp = self.zaehler
        self.zaehler = self.nenner
        self.nenner = temp
        return self

    def __neg__(self):
        """ Negation-Operator, when an fracture object has an minus before it, neg will make the fracture value negative

        :return: Negative fracture
        """
        self.zaehler = -self.zaehler
        return self

    def __pow__(self, power, modulo=None):
        """ Pow-Operator (**)

        :param power: Value of the power
        :param modulo: Not used
        :return: Returns the fracture with values to the given power
        """
        if isinstance(power, int):
            temp = Bruch(self.zaehler ** power, self.nenner ** power)
            return temp
        if isinstance(power, float):
                raise TypeError("Rationale Zahlen sind bei Potenzieren nicht erlaubt!")
        if isinstance(power, str):
                raise TypeError("Strings sind bei Potenzieren nicht erlaubt!")

    def __eq__(self, other):
        """ Equals-Operator (==), looks if two fracture-objects are identical

        :return: True when they are, else False
        """
        if isinstance(other, Bruch):
            if self.zaehler == other.zaehler and self.nenner == other.nenner:
                return 1
            else:
                return 0
        if isinstance(other, int):
            if self.zaehler == other and self.nenner == 1:
                return 1
            else:
                return 0

    def __ge__(self, other):
        """ Greater, equals than-Operator (>=), looks if the first fracture is greater or equal to the second one

        :return: True when it is, else False
        """
        if isinstance(other, Bruch):
            if float(self) >= float(other):
                return 1
            else:
                return 0
        if isinstance(other, int):
            if int(self) >= other:
                return 1
            else:
                return 0

    def __le__(self, other):
        """ Less, equals than-Operator (<=), looks if the first fracture is lesser or equal to the second one

        :return: True when it is, else False
        """
        if isinstance(other, Bruch):
            if float(self) <= float(other):
                return 1
            else:
                return 0
        if isinstance(other, int):
            if int(self) <= other:
                return 1
            else:
                return 0

    def __gt__(self, other):
        """ Greater than-Operator (>), looks if the first fracture is greater to the second one

        :return: True when it is, else False
        """
        if isinstance(other, Bruch):
            if float(self) > float(other):
                return 1
            else:
                return 0
        if isinstance(other, int):
            if int(self) > other:
                return 1
            else:
                return 0

    def __lt__(self, other):
        """ Less than-Operator (<), looks if the first fracture is lesser to the second one

        :return: True when it is, else False
        """
        if isinstance(other, Bruch):
            if float(self) < float(other):
                return 1
            else:
                return 0
        if isinstance(other, int):
            if int(self) < other:
                return 1
            else:
                return 0

    def _Bruch__makeBruch(value):
        """ Makes an fracture object from an integer value, 3 will be (3/1)

        :return: Fracture-object from an integer
        """
        if isinstance(value, int):
            return Bruch(value, 1)
        else:
            raise TypeError("Keine ganze Zahl übergeben")

    def gcd(a, b):
        """gcd calculates the Greatest Common Divisor of a and b.

        :return Greatest Common Divisor
        """
        while b:
            a, b = b, a % b
        return a

    def __iter__(self):
        """ With iter you can iterate over a fracture-object. In a fracture-object only the numerator and denominator are
             really important, so when you iterate once you get the numerator and the next time the denominator.
             To accomplish this, the keyword yield is used, instead of return, because it resume where it left off.

        :return: On the first iteration the numerator, on the second the denominator
        """
        yield self.zaehler
        yield self.nenner

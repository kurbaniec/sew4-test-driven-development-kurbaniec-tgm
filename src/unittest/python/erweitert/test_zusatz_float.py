"""
Created on 08.10.2018
@author: Kacper Urbaniec

Testet die neuen Float-Funktionen des Konstruktors
"""
import unittest
from bruch.Bruch import *


class TestFloatKonstruktor(unittest.TestCase):

    def setUp(self):
        self.b = Bruch(3, 1.1)
        self.b2 = Bruch(-3, -1.1)
        pass

    def testFloatKonstruktor1(self):
        self.b = Bruch(3, 1.1)
        assert(self.b == Bruch(30, 11))

    def testFloatKonstruktor2(self):
        self.b = Bruch(4.5)
        assert(int(self.b) == 4)

    def testFloatKonstruktor3(self):
        self.b = Bruch(1, 0.001)
        assert(int(self.b) == 1000)

    def testFloatKonstruktor4(self):
        assert(self.b == self.b2)

    def testFloatKonstruktorError(self):
        self.assertRaises(TypeError, Bruch, 2.0, "3.0")

    def testFloatKonstruktorError(self):
        self.assertRaises(TypeError, Bruch, "2.0", 3.0)

